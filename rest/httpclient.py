import time
import re
import json
import sys
from .bucket import Bucket
from requests import Request


class HttpClient(object):
    """
    The base Client to use for HTTP requests, handles route creation and rate-limiting
    """
    def __init__(self, token):
        self.token = token
        self.buckets = {}
        self.global_reset = None
        self.api_version = '6'
        self.api_url = 'https://discordapp.com/api/v{}'.format(self.api_version)

        py_version = '{}.{}.{}'.format(
            sys.version_info.major,
            sys.version_info.minor,
            sys.version_info.micro
        )
        self.headers = {
            'Authorization': 'Bot {}'.format(self.token),
            'User-Agent': 'DiscordBot (https://github.com/spec-tacles/spectacles.py) Python/{}'.format(py_version),
        }

    """
    globally_limited returns true if the client is globally rate-limited
    """
    def globally_limited(self):
        return False if self.global_reset is None else time.time() < self.global_reset

    """
    do creates an http request to the given path. the library auto prefixes the API url, encodes the
    request body, and decodes the response body
    """
    def do(self, method, path, body, files=[]):
        req = Request(method.upper(), self.api_url + path, headers=self.headers)
        route = make_route(method, path)
        if route not in self.buckets:
            bucket = Bucket(self, route)
            self.buckets[route] = bucket
        else:
            bucket = self.buckets.get(route)

        if len(files) > 0:
            req.data = {'payload_json': json.dumps(body)}
            if len(files) is 1:
                req.files = {'file': files[0]}
            else:
                req.files = {'file{}'.format(idx): tuple(i) for idx, i in enumerate(files)}
        else:
            req.data = json.dumps(body)
            req.headers['Content-Type'] = 'application/json'

        return bucket.do(req)


def make_route(method, route):
    url = route.split('?')[0]  # remove querystring parameters
    ids = re.findall('\d+', url)
    if len(ids) is 1:
        return url
    url = url.replace(url, ids[1], ':id', 1)
    # deleting messages has it's own bucket
    if method is 'DELETE' and 'messages' in url:
        return 'DELETE {}'.format(url)
    # reactions have their own bucket across the account
    if '/reactions/:id' in url:
        return '/channels/messages/:id/reactions'
    return url
