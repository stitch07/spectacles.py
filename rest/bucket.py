import time
import requests
from datetime import datetime


class Bucket:
    """
    Buckets represent rate-limit buckets
    """

    def __init__(self, client, route):
        self.client = client
        self.route = route
        self.session = requests.session()
        self.reset = None
        self.remaining = 1
        self.limit = 1

    def do(self, req):
        if self.client.globally_limited():
            wait = self.client.global_reset.time() - time.time()
            time.sleep(wait)

        if self.remaining <= 0:
            wait = self.reset - time.time()
            time.sleep(wait)
            self.remaining = self.limit

        res = self.session.send(req.prepare())
        if 'x-ratelimit-limit' in res.headers:
            self.limit = int(res.headers.get('x-ratelimit-limit'))
        if 'x-ratelimit-remaining' in res.headers:
            self.remaining = int(res.headers.get('x-ratelimit-remaining'))

        if res.status_code is 429:
            self.handle_429(res)
        elif res.status_code is 500:
            self.handle_500(res)
        else:
            self.handle_normal(res)
        return res.json()

    def handle_normal(self, res):
        if 'x-ratelimit-reset' not in res.headers:
            return
        reset_time = int(res.headers.get('x-ratelimit-reset'))

        if 'date' in res.headers:
            sent = datetime.strptime(res.headers.get('date'), '%a, %d %b %Y %H:%M:%S GMT')  # parse RFC1123 timestamp
        else:
            sent = datetime.now()

        diff = time.time() - sent.timestamp()
        self.reset = datetime.utcfromtimestamp(reset_time).timestamp() + diff

    def handle_429(self, res):

        body = res.json()
        reset = time.time() + body['retry_after']
        if 'global' in body:
            self.client.global_reset = reset
        else:
            self.reset = reset

    def handle_500(self, res):
        time.sleep(5)
        return self.do(res.request)
