import pika


class AMQP:
    def __init__(self, group, subgroup=''):
        self.group = group
        self.subgroup = subgroup
        self.channel = None
        self.__consumers__ = {}

    def connect(self, url):
        connection = pika.BlockingConnection(pika.ConnectionParameters(url))
        self.channel = connection.channel()
        self.channel.exchange_declare(
            exchange=self.group,
            durable=True,
        )

    def publish(self, event, data):
        self.channel.publish(self.group, event, data, properties=pika.BasicProperties(
            content_type='application/json',
        ))

    def subscribe(self, event, callback):
        sg = '{}:'.format(self.subgroup) if self.subgroup is not '' else ''
        queue_name = '{}:{}{}'.format(self.group, sg, event)
        self.channel.queue_declare(queue=queue_name, durable=True)
        self.channel.queue_bind(queue_name, self.group, routing_key=event)
        self.channel.basic_consume(callback)
